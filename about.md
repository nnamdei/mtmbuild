---
layout: page
title: About
permalink: /about/
show_in_nav: true
sitemap:
    priority: 1.0
    changefreq: weekly
---

We are community of technology enthusiasts made up of graphic designers, website developers, cyber security professionals, network administrators, digital marketers and everybody who loves Information Communication Technology in Niger State and the society at large. In this community of great minds, we are on a mission to improve the communication skills of its members, provide opportunities that will enhance their skill set and serve as a reliable platform where professionals can be consulted. We also believe that learning and sharing fun. 