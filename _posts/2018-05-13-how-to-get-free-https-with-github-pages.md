---
layout: post
title:  "How to get free HTTPS with Github pages"
date:   2018-05-13
categories: Github
excerpt_separator: "<!--more-->"
tags:
- Github

---
In the wake of recent uproar about some notable security breaches on the internet, the need for a robust security setup for our online presence cannot be over-emphasized. In the context of the prelude of this piece, <!--more-->I would like to remind us of the need of HTTPS (Secure Hyper Text Transfer Protocol) which enable connection between server and resource seeking user not to be compromised. In normal language, it helps to give users assurance that their information won’t be intercepted and used against their will. 
One of champions for advocacy of secured web platforms, Google recently added https support to third party domains using the blogger platform to stress their point to make the internet more safer. As we woke up to see the month of May, GitHub announced that their github pages will now offer https support to third party domains. Github pages is a tool that helps to quickly publish beautiful websites and other projects on low budget. It is distributed version control system that enables a user to write code offline, synchronize with the online copy while the abstraction (magic) is taken care of by Github website infrastructure. Just write your codes, init (initialize a git repo), commit, push, and your changes are live. GitHub Pages has supported custom domains since 2009, but didnt not offer https support to wesites that was not on the *.github.io domain. 
![Ingressive Campus Ambassador Meet-up]({{ "/img/github-pages-https-custom-domain.png" | absolute_url }})As from May 1, 2018, custom domains can be integrated on GitHub Pages and will gain support for HTTPS if you activate the right settings. HTTPS (most recognizable as the lock/padlock icon in your browser’s address bar) encrypts traffic between GitHub’s servers(where your files are uploaded to) and your browser giving you confidence that the page you asked for is the page you’re reading, from the site you think it is, and that others can’t snoop on or modify its contents along the way.
To activate this great feature on your project hosted with github pages;
Create a new repo or fork a repo that has html, css files
Click the settings tab 
For a forked project, rename the repository and;
Scroll down to GitHub Pages
Choose master as source 
Configuring your domain:
Type in the custom domain in the field provided and inform your domain registrar of this action to avoid errors or refer to this [Github setup guide](https://help.github.com/articles/setting-up-an-apex-domain/) to help in the setup of custom domain with Pages and update any A records you might have set. Dont forget to uphold the standards of the web as you provide solutions via the web.
