---
layout: post
title:  "Writing better Git commit messages"
date:   2019-4-12
categories: version control
excerpt_separator: "<!--more-->"
tags: 
- git
- version control
---

Its been awhile and i believe you have been awesome. As the second quarter of 2019 hits the nth day, it is our believe that you have taken some challenges and aced it! Progress is paramount in any field we find ourselves and as we strive to better we wish you the best things life can offer. <!--more-->With joy in our hearts, we would like to celebrate every person who has jumped on the wagon of version control system and still doing the evangelism of how awesome it can be. 
![writing better commit messages]({{ "/img/git_commit.png" | absolute_url }})We have seen a rise in the number of users of github which is evident in minna endpoint of github api which you can access [here](https://api.github.com/search/users?q=location:minna). Please note we celebrate 'little' feats like this too! With this in mind, let us do a recap of version control system in case you missed the memo, in simple terms, it is a system that help its users to keep track of the versions of product in case of any situation that may arise and some also use it as backup space for their files. Now we are somewhat aware of what VCS feels like, there is a challenge of using this system and this post will try to fix it. This challenge concerns the labelling of these versions that may arise as you create this product. For example, you are working with a team of software developers who have been assigned with specific tasks and you happen to be the supervisor or someone who reviews their work, how would you differentiate version 1 from 1.1 and 1.1.1? It comes in the form of commit messages whch describes the kind of changes made to the copy available of web apps like gitlab,github, bitbucket. It might be a challenge if you have changes happening at intervals that are not far apart and work has to be done from your end. So what is the solution to this? It comes in the form of using git cz and the rest is awesomeness wrapped in git *wink*. A friend, Mr. Umar shared this resource with me during one of our discussions and I took to it because of the styling it gives your commit message while not forgetting the icon too. To get started, you need to 

```download node js```  and 
execute ```npm install -g git-cz``` on our terminal or command prompt. 

Afterwards, follow the normal procedures of the git workflow(git init > git add > ...) but you have to replace **git commit -m "your-commit-message" with git cz thereafter follow the prompt using your arrow keys to navigate. As at the time of writing this post, git bash didnt respond to the navigation using arrow key and after you choose the classification of your commit message, you can skip other prompts by hitting the enter key until you need to answer the question on the terminal. I hope this post has helped you on how to write better commit messages like i try to do using git-cz. see you soon... 

Check out the repository on [Github](https://github.com/streamich/git-cz) and follow me [@nnamdei](https://github.com/nnamdei)