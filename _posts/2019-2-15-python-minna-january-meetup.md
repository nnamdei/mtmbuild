---
layout: post
title:  "Python Minna: January Meetup on Django re-usable apps"
date:   2019-2-15
categories: meetup
excerpt_separator: "<!--more-->"
tags: 
- django
- python minna
---

The stage was set and neutrals had alot going their minds concerning the subject matter. It was our first meetup (26/1/2019) in 2019 and one more step taken in the evangelism of python programming concepts. 
In case you missed the 'memo' in paragraph 1<!--more-->, I would like to refresh our minds about who we are in Niger State. we are a group that use the python programming language or its derivatives to provide solution to the world. We are known as Python Minna or Python User Group, Minna as known by Python Software Foundation.
![django reusable app meetup in minna]({{ "/img/django-reusable-apps-python-minna.jpg" | absolute_url }})
The theme for this event was Overview of django reusable application. Django is a framework built with Python Programming language that ensures that standard practices are followed in building websites. 'The' jango as some would call it, has a Model View Template architecture. It is a collection of python code/packages that is .maintained as an open source project. As an open source project, individuals can contribute to making it better so that identified bugs can be fixed or improvements implemented in the latest release which is hosted in the python package index directory.

The presentation on the overview of django reusable app was given by Mr. Nnamdi who spoke about django and significance of the topic for prospective and existing django apps developers. This reusable apps makes it possible for someone to create an app in a django project and make it available to the public or project-wise in line with DRY(don't repeat yourself) concept. We explored the basic requirements to create this reusable app which include a setup.py file, readme.rst file, a manifest.in file and not forgetting your python environment. We can find the slide to this document here or view the documentation here.

'The' jango might be tagged a high level framework but you should not allow such pedigree weigh you down in trying out its awesomeness when next you need to initialize a new project. Building and distributing Reusable apps is another way to improve your skills and your CV as a developer.

Before you head over to our [photo gallery](https://bit.ly/pythonminnaphoto), please keep in mind that as you build those 'little apps' you gain experience that is invaluable.

See you soon and tell a friend about the awesomeness of Python language.