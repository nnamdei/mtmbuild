---
layout: post
title:  "Flutter Live: Viewing Party in Minna, Niger State"
date:   2018-11-8
categories: meetup
excerpt_separator: "<!--more-->"
tags: 
- flutter
- flutter live
---

Lets say we a had flute that produce sounds after you put together some codes that is compiled... Pardon the high level jargon at the demise of the first sentence but its a prelude to what the awesome Flutter does for software developers. <!--more--> Flutter which is 'Made by Google' as stated [Here](https://flutter.io/) allows you to build beautiful native apps on iOS and Android from a single codebase. Flutter basically is a tool built with Dart Programming Language that helps solve some of the challenges in software development which includes learning multiple languages to deliver same product. ![Flutter-Live Minna]({{ "/img/flutterlive-minna-niger-state.png" | absolute_url }})One might be tempted to ask if there is a community of Flutter Developers in Niger State and the answer is yes. We supported a study session on Flutter with the Developer Student Club, Ibrahim Badamosi Babangida University, Lapai Chapter from August 31-September 1, 2018. During the two-day event that included aninstallation party and codelab, participants were assisted in setting up their computer systems as well as building their first projects. Flutter is growing in relevance in the ecosystem and come Decemeber 4, 2018 we are honoured to be hosting a viewing and listening party in Minna, Niger State. As a community we will join others all over to know latest news and updates about Flutter.

To be part of this great event, reserve your seat for this event and tell her friend about flutter. 

Event details: 

December 4,2018 - Viewing Party (5:00pm - 6:00pm)

December 5, 2018 - Listening Party, Talks and Codelabs (10:00am - 2:00pm)

Reserve your space [HERE](https://goo.gl/forms/17qpIATIX5YVq6OV2)


Feel free to share your experience so far using Flutter and learning Dart using the hashtag #FlutterLive and don't forget to mention @flutterio in your posts.

See you soon!!!