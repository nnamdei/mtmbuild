---
layout: post
title:  "Digital Disruption: Inclusion of Nigerian Women in Tech Driven Businesses"
date:   2018-06-27 18:20
categories: Women in Tech
excerpt_separator: "<!--more-->"
tags:
- women in technology
---

Africa has started riding on the new wave of a digital revolution. From the travel industry to retail, transportation and entertainment, digital disruption is changing the face of several industries. <!--more-->The retail industry has in recent times been rattled by the gradual shift to online shopping. Several brick and motar stores have been hit by this giant wave, resulting in the folding up of multiple businesses.
![nigerian women in technology]({{ "/img/nigerian-women-in-tech.jpeg" | absolute_url }})
Using the music industry as a case study, an industry that has enjoyed the rewards of digital disruption. A practical example, Sony went from selling vinyl record albums to selling CDs at walk-in stores and moved to the sales of digital music to costumers via smart phones. This type of “digital disruption” is hitting all industries and you have to find out whether you and your company are prepared for the transformation.
Another dramatic change in the last few years has been how companies now look forward to a different type of leadership. Several companies are now looking toward leaders who are willing to embrace new technologies. Therefore, it is imperative that today’s leaders be prepared (dynamic and forward thinking), as companies make a shift from traditional ways of doing business to undergoing a digital makeover.
According to several recent studies, despite the multiple calls from economic, political and societal experts to promote the inclusion of women in business, the future of many women in the workforce is uncertain.
While digital disruption means an increase in the number of opportunities, this also means a number of jobs are in line to be restructured or entirely replaced by technology in coming years, women in low skilled occupations such as sales are more likely to be impacted by automation and artificial intelligence technology.
Therefore it is necessary to equip and prepare women in business to understand the fundamentals of technology and gain new skill-sets which will keep them relevant in today’s workforce.
Although women are increasingly becoming key drivers in the rapid expansion and increasing development of a tech driven market and economy, there is still a long way to go. A few of the large tech driven companies like Omatek, Mainstreet Technologies and Jumia Group Nigeria boast of female Nigeria managing directors and CEOs.

Omatek (founded by the late Florence Seriki, the first Nigerian IT firm to be listed on the Nigerian Stock Exchange.)

Juliet Anammah is the CEO of Jumia Nigeria, Africa’s e commerce giant.

Omolara Adagunodo is the MD of Jumia Travel Nigeria, Africa’s largest hotel booking portal that connect you to hotels in Nigeria and several other countries across Africa.

Olamide Bada is the MD of Jumia Food, a food delivery service that connects you to restaurants in Nigeria and over 40 countries.

Funke Opeke of Mainstreet Technologies is the CEO of MainOne Cable Company, a submarine communications cable stretching from Portugal to South Africa with landings along the route in Accra , Ghana and Lagos, Nigeria.

Nimi Akinkugbe is the CEO of Bestman Games, the leading African games company and distributor of Hasbro games for over 40 African countries.

These are key examples of women who have risen against all odds to become the face of some of the giant technology driven brands in the country with influence across Africa.

The ability to adapt is critical in a constantly changing environment. Women should seek out mentors who have the knowledge base required to coach them in new technologies. So that when equipped, women can join the digital revolution and the world in making quicker, better and more informed decisions.

Written by:

Mariam Banwo Barry

