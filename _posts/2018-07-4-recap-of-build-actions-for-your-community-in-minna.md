---
layout: post
title:  "RECAP: Build Actions for your Community"
date:   2018-07-4 17:20
categories: BAFYC
excerpt_separator: "<!--more-->"
tags: 
- bafyc
---

In the words of Suleiman Lapai(President of JD Lab Nigeria Limited), the future is now and we ought to be active in its propagation. As the weather growled and threatened to ruin plans to build actions for our community event in Minna,<!--more--> the awesome people who turned up for this meetup made the organizers know of their intention to learn either in the rain or under the sun. 
![build actions for your community]({{ "/img/BAFYC-MINNA-MTM.jpg" | absolute_url }})The meet up was put together to raise awareness on an application called Google Assistant; which has been a work in progress for the team at Google Inc to improve interaction between virtual clients and humans, help participants create actions that can tackle a challenge in the society or increase engagement in their social circle. We had speakers who talked conversational patterns, 10 great things you can do with Google Assistant such setting reminders, biography search of popular figures, navigation and a practice session that involved the creation of actions that can help an individual know some facts about Niger State as he/she attempt trivia questions. It would be unfair to leave out the practice session done by our special guest NitroCoder who came all the way from Bida,Niger State to share his knowledge with this community. As we count down to the first year anniversary of this great community, we appreciate all the individuals who have in one way or the other contributed to the success of this community and we would continue to thrive for the advancement of ICT in our society and expose members experiences in line with best practice. We look forward to your contributions towards this community and send your message via our contact channels. Check out photos from the event here [Minna Technology Meetup Photo Album](http://bit.ly/mtmeetupphoto) 
  

