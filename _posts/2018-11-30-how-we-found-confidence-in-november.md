---
layout: post
title:  "How We Found Confidence in November"
date:   2018-11-30
categories: meetup
excerpt_separator: "<!--more-->"
tags: 
- minnatechmeetup
- meetup
---

How did we get here? some may be curious to know and the answer is 'we took a step sometime in 2017 and now we are here...'. The technology community in Niger State is gathering momentum and it is our hope that one day it will be a place to groom, RETAIN and sustain its own produce. <!--more-->The awareness about technology advancement in our society has been impressive in the last two years and can only get better. ![november-minna-meet-up]({{ "/img/minna-tech-meetup-november-2018.jpg" | absolute_url }})In a statistics that was released recently, users from Nigeria had an impressive contribution rate to and usage of the Git version control system which goes to show the interest from persons in this part of the world which to us 'is Awesome!'. In the midst of this revolution sweeping the land, we thought it necessary to evaluate our current status and proffer solutions that will tackle some challenges that are peculiar to the industry. The theme for our November meetup set the tone for Abubakar Garba and Nnamdi Okoro who gave brief presentations on Benefitting from the community and Confidence as a tool for self discovery respectively. The issue of confidence is one that can not be swept under the carpet because of its significance to the nature of this industry we find ourselves. There were interactions on the basis of the significance of balance in our endeavour to be good at what we do and other activities that might hinder our career path as well as the need for proper education of the essence of having a career in ICT. It was a great time with the community and we can only have an optimist outlook for the months ahead and we urge everyone reading this to be diligent and focused. Learning should be fun and you can checkout our album to see the memories we share as a community.


[Minna Technology Meetup Photo Album](http://bit.ly/mtmeetupphoto)

[Confidence as a tool for self discovery Slides](https://speakerdeck.com/namdi/confidence-as-a-tool-for-self-discovery) 

[Benefitting from the Community Slides](https://speakerdeck.com/sadiqful/benefitting-from-the-community) 

Feel free to share your experience using our hashtag #minnatechmeetup on social networks.

See you soon!!!