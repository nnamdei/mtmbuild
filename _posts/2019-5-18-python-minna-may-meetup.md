---
layout: post
title:  "Recap: Python Minna May Meetup"
date:   2019-5-18
categories: meetup
excerpt_separator: "<!--more-->"
tags: 
- django
- python minna
---

The anticipation to the our second meetup in 2019 was exciting going by the response to the event form despite the short notice. As follow up to our efforts in January,2019 where we talked about [Django Python](https://www.minnatechmeetup.com.ng/meetup/python-minna-january-meetup/) Framework used <!--more-->in building standard website applications, we decided to tag the meetup in May, 2019, libraries and scripting in python programming language. You might be wondering how 'we' came about; it is a collaboration between FUTMinna Developer Circle and Python User Group (Python Minna) that made this event happen and a synergy that will bring out the best from the technology community in the school environment.

We kicked off the event by 9:30am with opnening remarks from the Python Minna organizer, [Nnamdi Okoro](https://twitter.com/nnamdei) who had afew words of encouragement and an overview of the event to the 'early birds'. [Muhammad Aliyu](https://twitter.com/DR_CGURUSM) who is a co-lead of the [FUTMinna Developer Circle](https://twitter.com/futmxdevc) took us on the basics of the python programming using Jupyter notebook and somewhat made new friends for the Anaconda tool. ![django reusable app meetup in minna]({{ "/img/python-minna-may-meetup-2019.jpeg" | absolute_url }})It was a presentation that reminds us of genesis of python programming and one we can not get tired to referring to no matter one's level of knowledge or experience. [Khadijat Ladan](https://twitter.com/SLKhadeeja) is an Artificial Intelligence and Machine Learner enthusiast and a first time speaker talked about the libraries in python programming language using a simple approach to differentiate a library from framework had a codelab to back her presentation. She spoke about the essence of creating such library while stressing its significance in the python programming language ecosystem. Her presentation ended with a check list on how to publish scripts on PyPi and took questions on licensing and how to get a license for a script/package to be published on PyPi. 

There was time for first timers to try out Khadijat's code snippets and have a feel of being a python programmer before proceeded to [Mr. Abdulmalik Mustapha's](https://twitter.com/maleekcodes) presentation on Beginners Introduction to Machine Learning and who is a staff of Finche and student of Federal University of Technology, Minna. It was an interesting presentation for enthusiasts finding a path to follow while getting familiar with python programming language. There were instances cited by the presenter concerning its application in today's world and how efficient its result/output can be. We had [Peter Ejiga](https://twitter.com/ejigsonpeter), who is a software engineer and student of Federal University of Technology, Minna talk to us about computer vision and how the world is applying it to improve service delivery in businesses, curb incidences of crime and its relevance to humanity as it continues to gain ground.

It was a very insightful meetup for everybody who was in attendance due to the interaction initiated and work done by the presenters to dish out world class materials. We hit double figures in attendance and desires a kudos to everybody putting in the effort to sustain the energy amongst technology enthusiasts in this part of the world. Until we meet again, dont forget about the 'process' involved in attaining 'being awesome'. 

Check out our [photo gallery](https://bit.ly/pythonminnaphoto) to share in some moments captured during our meetup. 


