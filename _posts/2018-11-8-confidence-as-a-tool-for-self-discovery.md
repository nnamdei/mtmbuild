---
layout: post
title:  "Meetup: Confidence as a tool for Self Discovery"
date:   2018-11-8
categories: meetup
excerpt_separator: "<!--more-->"
tags: 
- minnatechmeetup
---

Whenever one is beset by a situation that he or she is unsure of – facing someone admired, having to perform in front of an audience, or simply talking to others – he or she is facing a goodly amount of stress. Confident people are usually able to face these situations without blinking; but the rest of us will probably melt away and try to run away from the situation.<!--more--> For most people facing this kind of low self-esteem, these situations present an opportunity for them to make fools out of themselves and can be a very embarrassing prospect. 

![confidence-Minna Meet-up]({{ "/img/confidence-minna-tech-meetup.jpg" | absolute_url }})
If you are one of the millions of people that would like to stop fidgeting in front of others, trying to squirrel out of such situations, and being so unsure of yourself when facing any challenge, here is another opportunity to hang out with like minds and listen to persons who have gone through what you feel is very un-natural or something un-speakable. 
Join us come 17th of November, 2018 as we talkless about codes and about growing as a person in terms of our confidence in our chosen path. The theme for this meetup is tagged "Confidence as a Tool for Self Discovery". 

Venue : Paritie Hub, C1, Minna GSM Village, Opposite Federal High Court,Western Bypass, Minna. Niger State.

Time: 10:00 am

Limited Seats are available but you Reserve a Seat [Here](http://bit.ly/mt-meetup)

See you soon!
