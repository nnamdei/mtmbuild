---
layout: post
title:  "Python Community meets Niger State"
date:   2018-06-10
categories: Python, NigerState
excerpt_separator: "<!--more-->"
tags:
- Python
- meetup

---
As the technology enthusiasm continues to gather momentum in Niger State, it was a matter of when rather than how a python community will be setup in this part of the world. <!--more-->Python Minna Meet up was held on Saturday 26th of May 2018 at Niger State Book and Other Intellectual Resource Development Agency, F-Layout, Minna. Niger State. It was an eagerly anticipated debut as enthusiasts of python software can have their say in a place like Minna, Niger State. The event was themed “Programming for Everybody” which set the agenda for the speakers to be subtle in the presentation. There were presentation on ‘Introduction to Python Programming’, ‘Introduction to Python for Cyber security’ and ‘Data Mining and Analysis with Python’ by Muhammad Ali, Raji Abdulgafar and David Joshua respectively. 

![Python Minna Meet-up]({{ "/img/python-minna.jpg" | absolute_url }})

During the presentation, the speakers engaged the participants with the hands on approach to help them have a better understanding of the subject matter. Each participant was assisted in installing the Python Software on their Laptops which enabled them follow the speakers during the practice session that included setting up a local server that can be used to share resources in events such as this Python Meet up, use of python installation package (pip) to install dependencies for the introduction to data mining presentation as well as interaction on the python console. Attendees were made to understand that programming with Python language is interesting and reminded that research and collaboration was paramount in our python community. It was encouraging as some attendees who were new to programming where enthusiastic about the possibilities with Python Programming and were grateful to the organizers of such meet up in the vicinity.
It was an event that raised awareness on the awesomeness of python software and dissuade individuals from the myth that programming is difficult. We would like to appreciate Python Software Foundation and Python Nigeria for the support towards the setting up a python community in Niger State and to all who have been part of this journey to make Minna and Niger State, the next silicon valley. It can only get better as we continue make life better with our selfless desire to create solutions with tools such as Python Software. 

Join our forum on [WhatsApp](https://chat.whatsapp.com/DQ5s5bZWeTbA6kIGUoP81Q) and check out photos of our first meetup here : [Python Minna Photo Album](http://bit.ly/pythonminnaphoto) 

